package ru.t1.ktubaltseva.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
@XmlAccessorType(XmlAccessType.FIELD)
public final class ProjectDTO extends AbstractUserOwnedModelWBSDTO {

    private static final long serialVersionUID = 0;

    public ProjectDTO(@Nullable String name) {
        super(name);
    }

}
