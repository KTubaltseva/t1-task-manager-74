package ru.t1.ktubaltseva.tm.dto.soap.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "userExistsByIdRequest")
public class UserExistsByIdRequest {

    @XmlElement(required = true)
    protected String id;

    public UserExistsByIdRequest(@NotNull final String id) {
        this.id = id;
    }

}
