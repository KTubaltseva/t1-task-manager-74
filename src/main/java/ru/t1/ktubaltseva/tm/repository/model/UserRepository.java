package ru.t1.ktubaltseva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.Optional;

@Repository
@Scope("prototype")
public interface UserRepository extends AbstractRepository<User> {

    Optional<User> findByLogin(@NotNull String login);

    Optional<User> findByEmail(@NotNull String Email);

    void deleteByLogin(@NotNull String login);

    void deleteByEmail(@NotNull String email);

    Boolean existsByLogin(@NotNull String login);

    Boolean existsByEmail(@NotNull String email);

}
