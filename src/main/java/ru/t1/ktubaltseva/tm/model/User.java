package ru.t1.ktubaltseva.tm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
public class User extends AbstractModel {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(name = "login", columnDefinition = "varchar(30)", nullable = false, unique = true)
    private String login = "";

    @NotNull
    @Column(name = "password_hash", columnDefinition = "varchar(255)", nullable = false)
    private String passwordHash = "";

    @Nullable
    @Column(name = "email", columnDefinition = "varchar(30)")
    private String email;

    @Nullable
    @Column(name = "first_name", columnDefinition = "varchar(30)")
    private String firstName;

    @Nullable
    @Column(name = "middle_name", columnDefinition = "varchar(30)")
    private String middleName;

    @Nullable
    @Column(name = "last_name", columnDefinition = "varchar(30)")
    private String lastName;

    @NotNull
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Role> roles = new ArrayList<>();

    @Column(name = "locked", nullable = false)
    private boolean locked = false;

    @NotNull
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    public User(@NotNull final String login, @Nullable final String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

    public User(@NotNull final String login, @NotNull final String passwordHash, @Nullable final String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        result += login;
        if (email != null)
            result += "\t" + email;
        return result;
    }

}
