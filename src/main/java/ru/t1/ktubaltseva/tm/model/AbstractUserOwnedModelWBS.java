package ru.t1.ktubaltseva.tm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.ktubaltseva.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Setter
@Getter
@MappedSuperclass
@NoArgsConstructor
public class AbstractUserOwnedModelWBS extends AbstractUserOwnedModel {

    @NotNull
    @Column(name = "name")
    protected String name = "";

    @Nullable
    @Column(name = "description")
    protected String description;

    @NotNull
    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "dateStart")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateStart;

    @Nullable
    @Column(name = "dateFinish")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    protected Date dateFinish;


    public AbstractUserOwnedModelWBS(@NotNull final String name) {
        this.name = name;
    }

    public AbstractUserOwnedModelWBS(
            @NotNull final String name,
            @Nullable final String description
    ) {
        this.name = name;
        this.description = description;
    }

    public AbstractUserOwnedModelWBS(
            @NotNull final User user,
            @NotNull final String name
    ) {
        this.user = user;
        this.name = name;
    }

    public AbstractUserOwnedModelWBS(
            @NotNull final User user,
            @NotNull final String name,
            @Nullable final String description
    ) {
        this.user = user;
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull String result = "";
        result += name;
        if (description != null)
            result += "\t(" + description + ")";
        result += "\t" + Status.toName(status) + "";
        return result;
    }

}
