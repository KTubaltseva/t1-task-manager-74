package ru.t1.ktubaltseva.tm.api.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.ktubaltseva.tm.dto.soap.task.*;
import ru.t1.ktubaltseva.tm.endpoint.soap.TaskSoapEndpoint;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public interface ITaskSoapEndpoint {

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskCountRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskCountResponse count(
            @RequestPayload @NotNull final TaskCountRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskClearRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskClearResponse clear(
            @RequestPayload @NotNull final TaskClearRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskDeleteByIdRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskDeleteByIdResponse deleteById(
            @RequestPayload @NotNull final TaskDeleteByIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskExistsByIdRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskExistsByIdResponse existsById(
            @RequestPayload @NotNull final TaskExistsByIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskFindAllResponse findAll(
            @RequestPayload @NotNull final TaskFindAllRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindAllByProjectIdRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskFindAllByProjectIdResponse findAllByProjectId(
            @RequestPayload @NotNull final TaskFindAllByProjectIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskFindByIdRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskFindByIdResponse findById(
            @RequestPayload @NotNull final TaskFindByIdRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskCreateRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskCreateResponse create(@RequestPayload @NotNull TaskCreateRequest request) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskAddRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskAddResponse add(
            @RequestPayload @NotNull final TaskAddRequest request
    ) throws AbstractException;

    @NotNull
    @ResponsePayload
    @PayloadRoot(localPart = "taskUpdateRequest", namespace = TaskSoapEndpoint.NAMESPACE)
    TaskUpdateResponse update(
            @RequestPayload @NotNull final TaskUpdateRequest request
    ) throws AbstractException;

}
