package ru.t1.ktubaltseva.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.model.UserDTO;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public interface IUserDTOService extends IDTOService<UserDTO> {

    @NotNull
    UserDTO findByLogin(@Nullable String login) throws AbstractException;

    @NotNull
    UserDTO findByEmail(@Nullable String email) throws AbstractException;

    @NotNull
    Boolean isLoginExists(@Nullable String login) throws AbstractException;

    @NotNull
    Boolean isEmailExists(@Nullable String email) throws AbstractException;

    @NotNull
    UserDTO lockUserByLogin(@Nullable String login) throws AbstractException;

    void removeByLogin(@Nullable String login) throws AbstractException;

    void removeByEmail(@Nullable String email) throws AbstractException;

    @NotNull
    UserDTO unlockUserByLogin(@Nullable String login) throws AbstractException;

}
