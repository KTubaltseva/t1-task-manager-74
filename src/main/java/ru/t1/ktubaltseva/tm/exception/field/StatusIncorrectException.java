package ru.t1.ktubaltseva.tm.exception.field;

import org.jetbrains.annotations.NotNull;

public class StatusIncorrectException extends AbstractFieldException {

    public StatusIncorrectException() {
        super("Error! Status is incorrect...");
    }

    public StatusIncorrectException(@NotNull final String value) {
        super("Error! This status value \"" + value + "\" is incorrect...");
    }

}