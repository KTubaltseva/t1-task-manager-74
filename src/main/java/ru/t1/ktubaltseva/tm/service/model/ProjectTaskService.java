package ru.t1.ktubaltseva.tm.service.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ktubaltseva.tm.api.service.model.IProjectTaskService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.repository.model.ProjectRepository;
import ru.t1.ktubaltseva.tm.repository.model.TaskRepository;

import java.util.List;

@Service
public class ProjectTaskService implements IProjectTaskService {

    @Getter
    @NotNull
    @Autowired
    private ProjectRepository projectRepository;

    @Getter
    @NotNull
    @Autowired
    private TaskRepository taskRepository;

    @Override
    @Transactional
    public void deleteByUserAndProject(
            @Nullable final User user,
            @Nullable final Project project
    ) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        if (project == null) throw new ProjectNotFoundException();
        if (project.getUser() == null) throw new ProjectNotFoundException();
        if (!project.getUser().getId().equals(user.getId())) throw new ProjectNotFoundException();
        getTaskRepository().deleteAllByUserAndProject(user, project);
        getProjectRepository().deleteByUserAndId(user, project.getId());
    }

    @Override
    @Transactional
    public void deleteAllByUser(@Nullable final User user) throws AbstractException {
        if (user == null) throw new UserNotFoundException();
        @NotNull final List<Project> projects = getProjectRepository().findAllByUser(user);
        for (@NotNull final Project project : projects) {
            getTaskRepository().deleteAllByUserAndProject(user, project);
        }
        getProjectRepository().deleteAllByUser(user);
    }

}

